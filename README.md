# Devops notes:

### Implemented features:
1. Jenkins pipeline for automatic provision and removal of the AWS environment using Terraform
2. Jenkins pipelines for automatic build and push of all microservices to ECR on merge/push to develop/master, unit and security tests for the auth-api microservice.
3. Fixed the existing 4-year-old k8s deployment files for manual deploys
4. Implemented helm charts for all microservices
5. Jenkins pipeline to automatically apply changes in EKS cluster when a new microservice version is tested and pushed to the ECR
6. Autoscaler for EKS worker nodes: https://prnt.sc/2131y03
7. Shared library for common jenkins groovy functions (for both Compito and Aufgabe projects)
8. Gitlab integration to track microservices' pipeline status
9. Versioning of feature branches builds using commit ID

### Repositories:

- https://gitlab.com/2meg/app_compito - the main repository with all files for all microservices (not used in the ci/cd process)
- https://gitlab.com/2meg/compito-k8s - repository with helm files that are needed for automatic deployment, k8s deployment files for manual deployment
- https://gitlab.com/2meg/tf_compito - repository with terraform files needed for the EKS cluster and surrounding infrastructure

### Microservice repositories (private; those represent microservice folders in this repository):

- https://gitlab.com/2meg/auth-api
- https://gitlab.com/2meg/frontend
- https://gitlab.com/2meg/log-message-processor
- https://gitlab.com/2meg/users-api
- https://gitlab.com/2meg/todos-api

### Continuous deployment stages: 
1. A separate Jenkins provisioning pipeline triggers the provision of the eks cluster and surrounding infrastructure, and also provides the Jenkins server with the credentials for the EKS cluster
2. After a commit or an accepted merge request to the develop branch, a build pipeline is triggered. All microservices belong to different repositories, so each repository has its own Jenkins pipeline. Auth-api microservice has additional security and unit test stages, other repositories (pipelines) have the "build" and "deploy" stages only.
3. Inside the build stage, a docker image gets built from the newly committed/merged changes and gets pushed to the microservice ECR repo with the "latest" tag.
4. The deploy stage on each pipeline starts a separate Jenkins pipeline called compito-k8s-deployment. The folder of this job in the main repo is "k8s". This pipeline applies helm charts using helmfile, and also restarts all deployment so that the new ecr images get pulled and deployed.
5. The application is accessible at the loadbalancer's DNS record, and the zipkin monitoring dashboard is available at its own loadbalancer's dns record + it's port (9411).
6. The infrastructure gets destroyed after the termination is confirmed on the provisioning pipeline

Video with the deployment process: 
https://www.youtube.com/watch?v=WVq92T26aZc&ab_channel=MyroslavS.

# Developer notes:

This is an example of web application comprising of several components communicating to each other. In other words, this is an example of microservice app. Why is it better than many other examples? Well, because these microservices are written in different languages. This approach gives you flexibility for running experiments in polyglot environment.

The app itself is a simple TODO app that additionally authenticates users. I planned to add some admin functionality, but decided to cut the scope and add it later if needed.

### Components

1. [Frontend](/frontend) part is a Javascript application, provides UI. Created with [VueJS](http://vuejs.org)
2. [Auth API](/auth-api) is written in Go and provides authorization functionality. Generates JWT tokens to be used with other APIs.
3. [TODOs API](/todos-api) is written with NodeJS, provides CRUD functionality over user's todo records. Also, it logs "create" and "delete" operations to Redis queue, so they can be later processed by [Log Message Processor](/log-message-processor).
4. [Users API](/users-api) is a Spring Boot project written in Java. Provides user profiles. Does not provide full CRUD for simplicity, just getting a single user and all users.
5. [Log Message Processor](/log-message-processor) is a very short queue processor written in Python. It's sole purpose is to read messages from Redis queue and print them to stdout
6. [Zipkin](https://zipkin.io). Optional 3rd party system that aggregates traces produced by other components.

Take a look at the components diagram that describes them and their interactions.
![microservice-app-example](https://user-images.githubusercontent.com/1905821/34918427-a931d84e-f952-11e7-85a0-ace34a2e8edb.png)

### Use cases

- Evaluate various instruments (monitoring, tracing, you name it): how easy they integrate, do they have any bugs with different languages, etc.

### How to start

The easiest way is to use `docker-compose`:

```
docker-compose up --build
```

Then go to http://127.0.0.1:8080 for web UI. [Zipkin](https://zipkin.io) is available on http://127.0.0.1:9411 by default.

### Contribution

This is definitely a contrived project, so it can be extended in any way you want. If you have a crazy idea (like RESTful API in Haskell that counts kittens of particular user) - just submit a PR.

### License

MIT
